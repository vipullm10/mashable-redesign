// HEADER SECTION
var navOpenBtn = document.getElementById('mobile-nav-open-btn');
var navCloseBtn = document.getElementById('mobile-nav-close-btn')
var mobileNav = document.querySelector('.mobile-nav-modal')
navOpenBtn.addEventListener('click',function(e){
    mobileNav.classList.add('visible');
});
navCloseBtn.addEventListener('click',function(e){
    mobileNav.classList.remove('visible');
});